%!TEX root = mathAppendix.tex
\subsection*{Description of data}

As said before, the absorbed energy by the PSII can be used into three different pathways (PSII, NPQ and NO). 
The energy partitioning can be described by the quantum yields of those processes: these are nonnegative and sum to one, $\phiNPQ + \phiNO + \phiPSII = 1 = 100\%$. 
The triplet $\phi = (\phiNPQ, \phiNO, \phiPSII)$ thus lies on a simplex and, as such, requires the application of specific statistical techniques, beyond standard multivariate regression, to describe its dependence on other available information (\cite{Aitchison1982,DirichletRegSoft,DirichletRegReport}).

We refer to $\phi$ as \textit{the response}, as is customary in the field of statistics (\cite{Hastie}).
Multiple responses have been observed in the experiment.
Each can be parametrized by three groups of descriptors, broadly characterized as spatial, temporal, or other.
Spatial descriptors include the plant number $p$, leaf number $l$, and pixel position $\pi$. 
Temporal descriptors include the day of the measurement $t_M$, and the age of a particular leaf $t_L$, also measured in days.
Other descriptors include the number of a pulse in the fluorescence measurement. 
We can thus parametrize a particular response as $\phi_{ p\,l\,\pi\, t_M\,t_L\,u}$, where $p$ is the plant number, $l$ is the leaf number (for a given plant), $\pi$ stands for pixel, and $u$ for pulse number. 
In order to simplify the response data, we decided to average out the obtained composition over all possible compositions obtained on a given leaf, $\phi = \phi_{p\,l\,t_M\,t_L\,u} = \sum_\pi \phi_{ p\,l\,\pi\,t_M\,t_Lu}/k$, where $k$ is the number of pixels for a given leaf in a given moment. 
The leaf-averaged response will be still referred to as a response.
This way we neglect entirely the variation of compositions within a particular leaf. 
This is a simplification that makes the procedures we use work faster. 
Also, due to the spatial resolution of the camera, the differences of compositions between nearby pixels within leaves tend to be small and their distribution is well approximated by the considered means. 
In general, the underlying statistical technique does not require this aggregation step and could be applied in presence of pixel-wise descriptors.

We assume that each observed value of the response $\phi_{p\,l\,t_M\,t_L\,u}$ depends on the interplay of the measured descriptors, a vector of values denoted by $x_{p\,l\,t_M\,t_L\,u}$. Typically, this part of data is called \textit{the control} (\cite{Hastie}). In the case of our experiment, the control can contain information on the plant and leaf age or some functions thereof: their specific form will be described later on. To ease the notation, denote the tuple $(p,l,t_M,t_L,u)$ by $i$. This way, instead $\phi_{p\,l\,t_M\,t_L\,u}$ we write $\phi_i$ and instead of $x_{plt_Mt_Lu}$, simply $x_i$. Moreover, in the next section, we will skip indexing $i$ at all, as it is not crucial for the exposition of the methods, and we will only return to it when needed. So, we will write $\phi$ for $\phi_i$ and $x$ for $x_i$.
