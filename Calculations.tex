%!TEX root = mathAppendix.tex
\subsection*{Calculations}
All calculations have been performed with freely available R package \url{DirichletReg} (\cite{DirichletRegReport, DirichletRegSoft}). 
Compared to another package, \url{dirmult} (\cite{dirmult}), this package enables the estimation using two different parametrizations of the Dirichlet distribution. 
This comes at the cost of a lack of support for additional penalties on the estimated parameters' value.

There exists a more commonly used parametrisation of the Dirichlet distribution than the one we use. It models directly the so called \textit{pseudocounts} $\alpha$ equal to $\nu\mu$, so that $\alpha = [\alpha_\text{NPQ}, \alpha_\text{NO}, \alpha_\text{PSII}]$ and $\alpha_\text{NPQ} = \phi\mu_\text{NPQ}, \alpha_\text{NO} = \phi\mu_\text{NO},$ and $\alpha_\text{PSII} = \phi\mu_\text{PSII}$. The only restriction on $\alpha$ is that its entries be non-negative. The commonly used link function between $\alpha$s and the linear model of covariates $x'\beta$ is the logarithm (compare with modelling the precision parameter $\nu$), so that $\log \alpha_m = x' \beta_m$, where $m$ is one of the de-excitation pathway tags. The above parametrisation does significantly change the maximization problem: in particular, it results in a different matrix of second derivatives of the logarithm of the likelihood described in Eq.~\ref{eq:likelihood}.

While considering both temporal controls in the sixth model it seems natural to ask if there is no significant correlation between the two variables that could lead to numerical instability of the presented models. The question is the more pendant considering the natural relationship between both variables, namely
$$ \text{Age of Leaf} = \text{Age of Rosette} - \text{Time of Appearance of a Leaf}.$$

Usually, collinearity might significantly distort the quality of the model, due to strictly technical reasons. The process of computation of standard deviations needed to establish the values of the z-statistics simply requires matrix inversion (by means of solving systems of multiple equations). Large errors might be inflicted upon this procedure due to the sensibility of the considered systems of equations to changes in their parameters. One way of obtaining upper bounds of this error is by means of the \textit{condition number}. A condition number assigns to a matrix $A$ a scalar value that measures how sensible is the solution of a system of equation $Ax=b$ to small differences in $b$ (\cite{Kincaid}). Since the values of the z-statistics are used in testing the irrelevance of particular covariates on the system, the numerical errors can severely compromise the statistical inference on the impact of parameters $(\beta_\text{NPQ}, \beta _\text{PSII}, \gamma)$  on the response.

We have observed empirically, that the use of the \textit{pseudocounts} parametrisation leads to values of $\kappa$ orders of magnitude higher than while using the direct parametrization in terms of $\mu$ and $\nu$. 
Our choice of parameters is also directly interpretable in terms of $\mu$ and $\nu$ as mentioned. 
Supplemental~Table~\ref{kappas} summarizes the condition numbers of different models.

\begin{table}[ht]
\centering
\begin{tabular}{cccccccc}
 Model 1 & Model 2 & Model 3 & Model 4 & Model 5 & Model 6 & Model 7 & Model 8 \\ \hline
 3.53 & 4.81 & 5.55 & 108.66 & 108.29 & 155.32 & 27.53 & 375.93 \\

\end{tabular}\caption{\textbf{The conditional numbers of estimated hessians of all considered models.} Higher values indicate potentially higher volatility of the estimates. The consistent results of the errors in the estimations obtained in the $k$-fold cross validation indicate that these higher bounds on the size of error are never achieved in practice on the considered data sets.}\label{kappas}
\end{table}

One can notice that the eight model has a relatively high condition number. However, it is still stable in the cross-validation tests, leading to consistently small errors. This lets us presume that the worst-case error in standard deviations retrieval that could occur due to higher conditional number is never achieved.

\FloatBarrier