%!TEX root = mathAppendix.tex
\subsection*{Dirichlet Regression}

We assume $\phi$ to be randomly drawn from the Dirichlet distribution. 
It is one of the simplest distributions defined over a simplex (\cite{Aitchison1982}). 
A vector $\phi$ following the Dirichlet distribution with expected value $\mu = [\mu_\text{NPQ}, \mu_\text{NO}, \mu_\text{PSII}]$ and precision $\nu$ has a probability density function given by
\begin{equation}
 d( \phi | \mu, \nu ) = \frac{\phi_\text{NPQ}^{\mu_\text{NPQ} \nu} \phi_\text{NO}^{\mu_\text{NO} \nu} \phi_\text{PSII}^{\mu_\text{PST} \nu}}{B(\mu, \nu)},
\end{equation}
where $B(\mu, \nu)$ is the normalizing factor. 
All parameters are non-negative. 
Additionally, $\mu_\text{NPQ} + \mu_\text{NO} + \mu_\text{PSII} = 1$, so that vector $\mu$ is itself a composition: the expected composition of the entire distribution. 
Hence, the distribution of $\phi$ is centered at $\mu$. 
The higher values of $\nu$ are, the more the distribution is localized around $\mu$.


To model the impact of the control variables on the response we assume that during an experiment only the most probable values of compositions are observed. 
This assumption is called the \textit{maximum likelihood principle} (\cite{Wasserman}).
Moreover, $x$ is assumed to impact $(\mu, \nu)$ in a specific way:
\begin{subequations}\label{eq:mean}
\begin{align}
 \mu_\text{NO}  &= \frac{1}{ 1 + e^{ x'A_\text{NPQ} } + e^{ x'A_\text{PSII} } } \\
 \mu_\text{NPQ}  &= \frac{ e^{ x'A_\text{NPQ} }}{ 1 + e^{ x'A_\text{NPQ} } + e^{ x'A_\text{PSII} } } \\
 \mu_\text{PSII} &= \frac{ e^{ x'A_\text{PSII} } }{ 1 + e^{ x'A_\text{NPQ} } + e^{ x'A_\text{PSII} } }.
\end{align}
\end{subequations}
Above, $x'A = \sum_{k = 1}^K x_k A_k$ is the scalar product of covariates $x$ and a vector of parameters $A$. $K$ is the total number of used covariates. 
The above parametrisation uses the \textit{multinomial logit link function} (\cite{MicrodataAnalysis}). 
Observe that the above equations sum to one. 
One can rearrange these into two independent equations that directly model the impact of $x$ on the logarithms of the relative changes in the average levels of specific quantum yields,
\begin{subequations}\label{eq:precision}
\begin{align}
 \log \frac{ \mu_\text{NPQ}  }{ \mu_\text{NO} } &= x'A_\text{NPQ} \\
 \log \frac{ \mu_\text{PSII} }{ \mu_\text{NO} } &= x'A_\text{PSII}.
\end{align}
\end{subequations}
Therefore, the right hand side of the equations describe the relative levels of quantum yields of PSII and regualted NPQ in terms of the quantum yield of non-regulated NPQ. 
In particular, the more both $x'A_\text{NPQ}$ and $x'A_\text{PSII}$ are below zero, the more energy will end up on the $\Phi_\text{NO}$ channel.

Finally, we model the logarithm of the precision parameter as
\begin{align}
  \log \nu &= G^t x.
\end{align}
Thus, the larger the values of $G^t x$, the more compositions should be close to the mean.

Having described the dependence between a particular response $\phi$ and control $x$ we will now describe how to link the information on the parameters from different measurements: we reintroduce the indices of the data points in the statistical sample. We assume that the errors in quantum yields measurements $\phi_i$ were independent given some underlying common set of parameters parameters $\Theta = (A_\text{NPQ}, A_\text{PSII}, G)$ and their respective values of controls $x_i$. The choice of one common $\Theta$ for all sample points $i$ reflects our belief that a single \textit{data generation process} is capable of describing the studied phenomenon of energy partitioning. The probability of the observed measurements can therefore be represented as
\begin{equation}\label{eq:likelihood}
 L = \prod_{i} \frac{
  \phi_{i,\text{NPQ}}^{\mu_\text{NPQ}(x_i | \Theta) \nu(x_i | \Theta) } \phi_{i,\text{NO}}^{\mu_\text{NO}(x_i | \Theta) \nu(x_i | \Theta)} \phi_{i,\text{PSII}}^{\mu_\text{PST}(x_i | \Theta) \nu(x_i | \Theta)}
 }{
  B\big(\mu(x_i | \Theta), \nu(x_i | \Theta)\big)
 }.
\end{equation}
Following the \textit{maximum likelihood principle}, the above expression is maximised with respect to parameters $\Theta$. The outcome of such procedure, denoted $\Theta^*$, describes the model that best fits the data, given the choice of descriptors $x_i$. 

As we shall see in the next section, there are many valid ways of representing the information gathered on the age of plant and the age of a leaf. For each such way, the above procedure will render a different set of optimal parameters $\Theta^*$. These might even vary in dimension. Among these models, we will choose the best one using a different criterion. The above procedure is technically referred to as \textit{model selection}, see (\cite{Hastie}). In the next section, we will precise how to link the control to the response variables so that the whole procedure could be applied to unveil biologically relevant information.