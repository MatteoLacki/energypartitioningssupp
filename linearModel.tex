%!TEX root = mathAppendix.tex
\subsection*{The modelling strategy}

Here we precise the form of the linear models $x'A_\text{NPQ}, x'A_\text{PSII}, x'G$.
The available control variables are the age of the plant $t_R$, the age of leaf $t_L$, and both the pulse and the plant numbers.
Different ways of encoding this information lead to different models.
We will introduce the models from the least complex one, following Figure~7 in the main text.

To compare the models we use two widely accepted methods: the asymptotic likelihood ratio test (\cite{Wasserman}) and the k-fold cross-validation, as described in the manuscript.


\subsection*{The models}

In this paper we make a simplified assumption that both expressions $\log \frac{ \mu_\text{NPQ}  }{ \mu_\text{NO} } $ and $\log \frac{ \mu_\text{PSII} }{ \mu_\text{NO} }$ have the same algebraic form.

The simplest model neglects all the information gathered in the control variables. It can be represented as:
\begin{equation*}
  \text{ \textcolor{mc}{MODEL $1$:} }
  \begin{cases}
    \log \frac{ \mu_\text{NPQ}  }{ \mu_\text{NO} } =
    \beta^\text{NPQ}, \quad
    \log \frac{ \mu_\text{PSII} }{ \mu_\text{NO} } =
    \beta^\text{PSII}, \quad
    \log \nu = \gamma.
  \end{cases}
\end{equation*}

The sampled energy compositions tend to localize in a narrow region: otherwise, the resulting cross-validation error would not attain the relatively small value of 13.9\% of the maximal error, see Figure~7 in the main text.
Not surprisingly, this model has the highest error rate among the models we consider.
We thus treat the error-rate of 13.9\% should be treated as the reference.
Adding extra terms to the model enhances its capability to fit the data.

\textcolor{mc}{MODEL $1$:} can be improved by introducing the effect of pulse on the average relative levels of energy partitioning,
\begin{equation*}
\text{ \textcolor{mc}{MODEL $2$:} }
  \begin{cases}
  \log \frac{ \mu_\text{NPQ}  }{ \mu_\text{NO} } =
    \beta^\text{NPQ}_u,\quad
    \log \frac{ \mu_\text{PSII} }{ \mu_\text{NO} } =
    \beta^\text{PSII}_u,\quad
    \log \nu = \gamma.
  \end{cases}
\end{equation*}
The big difference in log-likelihoods of the above models provides evidence against the null hypothesis $H_0$.
$H_0$ states that there is no difference between the more advanced model and the one constrained by equation $\beta_u = \beta$.
Also, Figure~7 presents a noticeable drop in the average cross-validation error rate from $13.9\%$ to $4.9\%$ -- more than $2.8$ fold.
\textcolor{mc}{MODEL $2$}  thus makes on average 2.8 fewer errors than its predecessor.

Even in homozygous WT material, plants differ slightly during the growth.
To catter for this phenotypic variation, we constructed a model whose parameters are plant-specific,
\begin{equation*}
\text{ \textcolor{mc}{MODEL $3$:} }
  \begin{cases}
  \log \frac{ \mu_\text{NPQ}  }{ \mu_\text{NO} } =
    \beta^\text{NPQ}_{pu},\quad
    \log \frac{ \mu_\text{PSII} }{ \mu_\text{NO} } =
    \beta^\text{PSII}_{pu},\quad
    \log \nu = \gamma_p.
  \end{cases}
\end{equation*}
The difference between this model and its predecessor is much less pronounced in absolute terms both in terms of the cross-validation error (equal roughly to one permille) and the value of the difference of likelihoods.
Both models also differ by less than one percent of the error rate of the trivial reference model.

The models we are now to describe are examining the impact of other control variables on energy partitioning.
In view of \textcite{Carvalho2015}, first, we assessed the individual effect of the age of the plant.
For that purpose we have built the fourth model,
\begin{equation*}
\text{ \textcolor{mc}{MODEL $4$:} }
  \begin{cases}
    \log(\mu_\text{NPQ}/\mu_\text{NO} )   &= \beta^\text{NPQ}_{pu} + \eta^\text{NPQ} t_R \\
    \log(\mu_\text{PSII}/\mu_\text{NO} )  &= \beta^\text{PSII}_{pu} + \eta^\text{PSII} t_R\\
    \log \nu &= \gamma_p.
  \end{cases}
\end{equation*}
The alternative would be to include the age of leaves alone, which constitutes the fifth model,
\begin{equation*}
\text{ \textcolor{mc}{MODEL $5$:} }
  \begin{cases}
    \log \frac{ \mu_\text{NPQ}  }{ \mu_\text{NO} } &= \beta^\text{NPQ}_{pu} + \zeta^\text{NPQ} t_L, \\
    \log \frac{ \mu_\text{PSII} }{ \mu_\text{NO} } &= \beta^\text{PSII}_{pu} + \zeta^\text{PSII} t_L,\\
    \log \nu &= \gamma_p.
  \end{cases}
\end{equation*}
The two models are not directly comparable using the log-likelihood test (at least in the test's simplest form). However, both models are more complex than model three. Also, both models are nested in the more complex model number six,
\begin{equation*}
\text{ \textcolor{mc}{MODEL $6$:} }
  \begin{cases}
    \log \frac{ \mu_\text{NPQ}  }{ \mu_\text{NO} } &= \beta^\text{NPQ}_{pu} + \eta^\text{NPQ} t_R + \zeta^\text{NPQ} t_L, \\
    \log \frac{ \mu_\text{PSII} }{ \mu_\text{NO} } &= \beta^\text{PSII}_{pu} + \eta^\text{PSII} t_R + \zeta^\text{PSII} t_L,\\
    \log \nu &= \gamma_p.
  \end{cases}
\end{equation*}
This model has the quality of considering both time components together: if we assume either that $\zeta = 0$ or $\eta = 0$ we retrieve respectively models four and five. There are several reasons why one should not include these contraints:
\begin{enumerate}
  \item The cross-validation error drops when both temporal control variables are included in the model, as seen in Figure~7 in the main manuscript: the difference between model six and three roughly equals $1.2\%$.
  \item The likelihood ratio test results are highly significant: all p-values are below $2.2 \times 10^{-16}$.
  \item The marginal p-values of the estimates of $\zeta$ and $\eta$ in model six are all below $2 \times 10^{-16}$, suggesting they both have an impact on explaining the response variables.
\end{enumerate}
Therefore, we deem the sixth model superior to the previously described ones.

The answer to the posed question on the impact of both temporal controls is thus not simple.
It is interesting to note that both quality metrics seem to indicate that considering the leaf age offers a slight advantage over the plant age.
However, \textcolor{mc}{MODEL $4$} and \textcolor{mc}{MODEL $5$} are both inferior to \textcolor{mc}{MODEL $6$}.
This indicates that both temporal covariates should be considered together.
As seen Figure~8 in the main manuscript, \textcolor{mc}{MODEL $4$} and \textcolor{mc}{MODEL $5$} attain around 30\% of the error-rate of the first model, while for model \textcolor{mc}{MODEL $6$} this is only 25\%.

Exploratory analysis underlined the importance of pulse number on the quantum yields (Figure 1, 3-5).
This led us to consider the seventh model, where the parameters next to the temporal variables can explicitly depend upon the pulse number,
\begin{equation*}
\text{ \textcolor{mc}{MODEL $7$:} }
  \begin{cases}
    \log \frac{ \mu_\text{NPQ}  }{ \mu_\text{NO} } &= \beta^\text{NPQ}_{pu} + \eta_{u}^\text{NPQ} t_R + \zeta_{u}^\text{NPQ} t_L , \\
    \log \frac{ \mu_\text{PSII} }{ \mu_\text{NO} } &= \beta^\text{PSII}_{pu} + \eta_{u}^\text{PSII} t_R + \zeta_{u}^\text{PSII} t_L,\\
    \log \nu &= \gamma_p.
  \end{cases}
\end{equation*}
The cross-validation error dropped to the level of $23.74\%$ of the error rate of \textcolor{mc}{MODEL $1$}.
Another highly significant difference in log-likelihoods was observed between this model and its predecessor.

Finally, we extend the model beyond linearity in the final, eighth model,
\begin{equation*}
\text{ \textcolor{mc}{MODEL $8$:} }
  \begin{cases}
    \log \frac{ \mu_\text{NPQ}  }{ \mu_\text{NO} } &=
    \beta^\text{NPQ}_{pu} + \eta_{u}^\text{NPQ} t_R + \zeta_{u}^\text{NPQ} t_L + \xi_{u}^\text{NPQ} t_R^2 + \delta_{u}^\text{NPQ} t_L^2 + \theta_{u}^\text{NPQ} t_R t_L \\
    \log \frac{ \mu_\text{PSII} }{ \mu_\text{NO} } &=
    \beta^\text{PSII}_{pu} + \eta_{uR}^\text{PSII} t_R  + \zeta_{u}^\text{PSII} t_L + \xi_{u}^\text{PSII} t_R^2 + \delta_{u}^\text{PSII} t_L^2 + \theta_{u}^\text{PSII} t_R t_L\\
  \log \nu &= \gamma_p.
  \end{cases}
\end{equation*}
There is again a small, significant difference between \textcolor{mc}{MODEL $8$} and the nested \textcolor{mc}{MODEL $7$}.

The estimated coefficients of the model are all gathered in Supplemental~Tables~\ref{bigModelNPQestim}-\ref{bigModelPrecisionEstim}.
Additionally, we visualise the estimated coefficients from Supplemental~Table~\ref{bigModelNPQestim} and Supplemental~Table~\ref{bigModelPSIIestim} in
Supplemental~Figure~4.
% Supplemental~\ref{model8coefs}.

The biggest role in explaining the phenomenon can be attributed to the terms not related to the temporal variables, i.e. $\eta, \zeta, \xi, \delta, \theta$  
(Supplemental~Figure~4A). % (Figure~\ref{model8coefs}~A). 

With consecutive pulses values of most of the coefficients seem to converge to limiting values.
This reflects the tendency of the plant to stabilize its response to sudden light exposure.
Observe that after Pulse 2 several parameters change signs, underlining the abrupt change between Pulse 1 and Pulse 2 mentioned in the main manuscript.
Also, the quadratic and mixed terms of the model ($\xi, \delta, \theta$) are orders of magnitude smaller than linear terms ($\zeta, \eta$); see Supplemental~Figure~\ref{model8coefs}B.
This means that only for older leaves and for an older plant ($\eta, \zeta > 0$), will there be any significant difference between \textcolor{mc}{MODEL $7$} and \textcolor{mc}{MODEL $8$}.
In absolute terms the values of coefficients attributed to leaf age are always bigger than those of plant age when modelling $\log \frac{ \mu_\text{NPQ}  }{ \mu_\text{NO} }$
and smaller when modelling $\log \frac{ \mu_\text{PSII}  }{ \mu_\text{NO} }$ for Pulses 2 to 7.
In Pulse 1 the leaf age effect is always bigger in absolute terms.
Also, the effects are usually adverse, the leaf age tending to decrease both modelled quantities, with the exception of Pulse 6 and 7 while modelling $\log \frac{ \mu_\text{NPQ} }{ \mu_\text{NO} }$.

\begin{figure}[ht]
  \centering
  \includegraphics[width=0.8\linewidth]{plots/coefPlotComplex.png}
  \caption{\textbf{Estimated coefficients of the \textcolor{mc}{MODEL $8$}}. The upper row plots (A) represents all estimated coefficients for the two equations of interest. The lower row plots (B) zoom into the yellowish strip region of the plot (A), filtering out coefficients not related to temporal control variables, i.e. of the form $\beta_{pu}$. One can note the tendency of the estimates to stabilize with pulse numbers, which shows that the leaves attend the equilibrium point after several rounds of exposure to light pulses: this is especially the case of the coefficients $\beta_{pu}$ that are chiefly responsible for the positioning of the compositions on the simplex.
  Plot (B) shows that the leaf age and plant age have typically adverse effects on the relative ratios of the quantum yields: in general, the leaf age tends to favor other processes ($\mu_\text{NO}$) over the regulated thermal energy dissipation ($\mu_\text{NPQ}$) and over the photochemical conversion ($\mu_\text{PSII}$). }\label{model8coefs}
\end{figure}

\subsection*{Further inquiry into \textcolor{mc}{MODEL $8$}}
Cross-validation indicated that \textcolor{mc}{MODEL $8$} is the best one to describe the phenomenon, with an average error rate equal to $3.1\%$ of the maximal attainable error.
In Supplemental~Figure~\ref{modelErrors} we plot in form of a vector field the differences between the predictions of the \textcolor{mc}{MODEL $8$} and the actual values of the energy partitioning.
Vectors originate at the predicted values and their heads point towards the experimentally observed values.
The longer the vector is, the bigger the error gets.
This is additionally encoded in color intensity, where red values show bigger errors.
The figure indicates that in general, the predicted values follow well the true values (as the blue color dominates).
We can also notice different plants seem to follow similar patterns of changes during different pulses.
To simplify the analysis of errors show in Supplemental~Figure~\ref{modelErrors}, we have decided to reposition the vectors and attach their origins to one point in the middle of each ternary plot, see Supplemental~Figure~\ref{errorDistribution}.

To summarize the distribution of the heads, contour plots are plotted in Supplemental~Figure~\ref{errorDistribution}.
These depict borders of regions that contain respectively 80, 60, 40 and 20\% of all arrow heads (plotted in violet, cyan, green, and red).
They represent both the direction and the extent of the error \textcolor{mc}{MODEL $8$} makes.
Note that the estimates are mostly unbiased, with a limited variance.
Also, the distribution localizes in elliptically shaped regions.
This result can be traced to asymptotic distribution of the maximum likelihood estimates: it the limit it should be normal, centered at true values of the investigated parameters, and with a covariance matrix equal to the inverse of the Fisher information matrix, see \textcite{Wasserman}.

To clarify the role of the model and its filtering capacities, we compared the real data with the model predictions for different ages of the plant and leaf (Supplemental~Figure~\ref{filteringInTemporal}). The model poorly handles top right edges of the triangles. At the end of the experiment, there are altogether not too many leaves from the beginning that survive or are not covered by other leaves.

\begin{landscape}
\begin{figure}[p]
  \centering
  \includegraphics[width=1.05\linewidth]{plots/Errors2.pdf}
  \caption{\textbf{\textcolor{mc}{MODEL $8$} predictions versus the actual measurements for different combination of pulse numbers and plants.} A given pair $(prediction, observation)$ is depicted as a vector attached to the value predicted by the model and with head ending at the corresponding observation. The longer the vector, the bigger the error. Additionally, error is also mapped to color intensity, according to the legend on the right that reports the error in percentage points.}\label{modelErrors}
\end{figure}
\end{landscape}

\begin{landscape}
\begin{figure}[p]
  \centering
  \includegraphics[width=1.05\linewidth]{plots/pureErrorPlot.pdf}
  \caption{\textbf{The distribution of errors of the \textcolor{mc}{MODEL $8$} for different combination of pulse numbers and plants.} Each gray vector corresponds to precisely one vector in Supplemental~Figure~\ref{modelErrors}. All vectors have been repositioned so that the \textit{prediction ending} can be found at the center of the plot (red dot). The distribution of the other endings is summarized by the contour plots that show approximate regions containing a given percentage of the vectors' heads, check the legend for details. Black segments correspond to a $5$ percent points error and are given for reference.}\label{errorDistribution}
\end{figure}
\end{landscape}

\begin{figure}[p]
  \centering
  \includegraphics[width=\linewidth]{plots/comparisonDark.png}
  \caption{\textbf{Levels of energy partitions as a function of plant and leaf age.} Energy levels are mapped to color intensity according to legends to the right. Top panel present values directly retrieved from the data. The bottom panel shows prediction of \textcolor{mc}{MODEL $8$}. }\label{filteringInTemporal}
\end{figure}



\begin{landscape}
\begin{table}[p]
\scriptsize
\centering
\begin{tabular}{lrrrllrrrllrrrl}
& Estimate & St.Err. & p-value &&& Estimate & St.Err. & p-value &&& Estimate & St.Err. & p-value & \\
  \hline
$\beta^{NPQ}_{u_1\,p_1}$ & -1.72164 & $70.3\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\beta^{NPQ}_{u_6\,p_5}$ & 0.72963 & $42.1\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\zeta^{NPQ}_{u_4}$ & -0.03561 & $3.6\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ \\
  $\beta^{NPQ}_{u_2\,p_1}$ & 0.24481 & $37.4\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\beta^{NPQ}_{u_7\,p_5}$ & 0.75953 & $42.3\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\zeta^{NPQ}_{u_5}$ & -0.03321 & $3.6\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ \\
  $\beta^{NPQ}_{u_3\,p_1}$ & 0.53226 & $39.8\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\eta^{NPQ}_{u_1\,R}$ & -0.06928 & $6.3\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\zeta^{NPQ}_{u_6}$ & -0.03818 & $3.6\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ \\
  $\beta^{NPQ}_{u_4\,p_1}$ & 0.61426 & $41\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\eta^{NPQ}_{u_2}$ & 0.03723 & $3.3\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\zeta^{NPQ}_{u_7}$ & -0.04275 & $3.6\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ \\
  $\beta^{NPQ}_{u_5\,p_1}$ & 0.68850 & $41.7\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\eta^{NPQ}_{u_3}$ & 0.02444 & $3.5\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\delta^{NPQ}_{u_1}$ & -0.00284 & $< 0.5\,\text{\textperthousand} $ & $< 0.5\,\text{\textperthousand} $ & $***$ \\
  $\beta^{NPQ}_{u_6\,p_1}$ & 0.75426 & $41.9\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\eta^{NPQ}_{u_4}$ & 0.01370 & $3.6\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\delta^{NPQ}_{u_2}$ & -0.00084 & $< 0.5\,\text{\textperthousand} $ & $< 0.5\,\text{\textperthousand} $ & $***$ \\
  $\beta^{NPQ}_{u_7\,p_1}$ & 0.79163 & $42.1\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\eta^{NPQ}_{u_5}$ & 0.00142 & $3.6\,\text{\textperthousand}$ & $696.8\,\text{\textperthousand}$ & $$ & $\delta^{NPQ}_{u_3}$ & -0.00066 & $< 0.5\,\text{\textperthousand} $ & $< 0.5\,\text{\textperthousand} $ & $***$ \\
  $\beta^{NPQ}_{u_1\,p_3}$ & -1.82698 & $70\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\eta^{NPQ}_{u_6}$ & -0.00848 & $3.7\,\text{\textperthousand}$ & $20.9\,\text{\textperthousand}$ & $*$ & $\delta^{NPQ}_{u_4}$ & -0.00048 & $< 0.5\,\text{\textperthousand} $ & $< 0.5\,\text{\textperthousand} $ & $***$ \\
  $\beta^{NPQ}_{u_2\,p_3}$ & 0.25678 & $37.4\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\eta^{NPQ}_{u_7}$ & -0.01566 & $3.7\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\delta^{NPQ}_{u_5}$ & -0.00035 & $< 0.5\,\text{\textperthousand} $ & $1.8\,\text{\textperthousand}$ & $**$ \\
  $\beta^{NPQ}_{u_3\,p_3}$ & 0.60083 & $39.7\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\xi^{NPQ}_{u_1}$ & 0.00076 & $< 0.5\,\text{\textperthousand} $ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\delta^{NPQ}_{u_6}$ & -0.00006 & $< 0.5\,\text{\textperthousand} $ & $619.5\,\text{\textperthousand}$ & $$ \\
  $\beta^{NPQ}_{u_4\,p_3}$ & 0.70463 & $41\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\xi^{NPQ}_{u_2}$ & -0.00093 & $< 0.5\,\text{\textperthousand} $ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\delta^{NPQ}_{u_7}$ & 0.00020 & $< 0.5\,\text{\textperthousand} $ & $71.9\,\text{\textperthousand}$ & $.$ \\
  $\beta^{NPQ}_{u_5\,p_3}$ & 0.80243 & $41.7\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\xi^{NPQ}_{u_3}$ & -0.00066 & $< 0.5\,\text{\textperthousand} $ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\theta^{NPQ}_{u_1}$ & 0.00126 & $< 0.5\,\text{\textperthousand} $ & $< 0.5\,\text{\textperthousand} $ & $***$ \\
  $\beta^{NPQ}_{u_6\,p_3}$ & 0.87495 & $41.9\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\xi^{NPQ}_{u_4}$ & -0.00047 & $< 0.5\,\text{\textperthousand} $ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\theta^{NPQ}_{u_2}$ & 0.00184 & $< 0.5\,\text{\textperthousand} $ & $< 0.5\,\text{\textperthousand} $ & $***$ \\
  $\beta^{NPQ}_{u_7\,p_3}$ & 0.90785 & $42.1\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\xi^{NPQ}_{u_5}$ & -0.00027 & $< 0.5\,\text{\textperthousand} $ & $0.7\,\text{\textperthousand}$ & $***$ & $\theta^{NPQ}_{u_3}$ & 0.00121 & $< 0.5\,\text{\textperthousand} $ & $< 0.5\,\text{\textperthousand} $ & $***$ \\
  $\beta^{NPQ}_{u_1\,p_5}$ & -1.78330 & $69.6\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\xi^{NPQ}_{u_6}$ & -0.00010 & $< 0.5\,\text{\textperthousand} $ & $194.5\,\text{\textperthousand}$ & $$ & $\theta^{NPQ}_{u_4}$ & 0.00074 & $< 0.5\,\text{\textperthousand} $ & $< 0.5\,\text{\textperthousand} $ & $***$ \\
  $\beta^{NPQ}_{u_2\,p_5}$ & 0.19882 & $37.6\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\xi^{NPQ}_{u_7}$ & 0.00002 & $< 0.5\,\text{\textperthousand} $ & $778.8\,\text{\textperthousand}$ & $$ & $\theta^{NPQ}_{u_5}$ & 0.00071 & $< 0.5\,\text{\textperthousand} $ & $< 0.5\,\text{\textperthousand} $ & $***$ \\
  $\beta^{NPQ}_{u_3\,p_5}$ & 0.48652 & $40\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\zeta^{NPQ}_{u_1}$ & 0.04036 & $6\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\theta^{NPQ}_{u_6}$ & 0.00069 & $< 0.5\,\text{\textperthousand} $ & $< 0.5\,\text{\textperthousand} $ & $***$ \\
  $\beta^{NPQ}_{u_4\,p_5}$ & 0.57890 & $41.2\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\zeta^{NPQ}_{u_2}$ & -0.07441 & $3.3\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\theta^{NPQ}_{u_7}$ & 0.00066 & $< 0.5\,\text{\textperthousand} $ & $< 0.5\,\text{\textperthousand} $ & $***$ \\
  $\beta^{NPQ}_{u_5\,p_5}$ & 0.66365 & $41.9\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\zeta^{NPQ}_{u_3}$ & -0.05214 & $3.5\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & \\
   \hline
\end{tabular}\caption{\textbf{Results of estimating the parameters of \textcolor{mc}{MODEL $8$}.} In consecutive columns we find (repeatedly): name of the coefficient, its estimated value, the standard deviation of the estimate, the p-value of the null hypothesis that assumes that a given parameter has no impact on the response variable (precised in the superscript).}\label{bigModelNPQestim}
\end{table}
\end{landscape}

% $\log \frac{ \mu_\text{NPQ}  }{ \mu_\text{NO} }$
\begin{landscape}
\begin{table}[p]
\centering
\scriptsize
\begin{tabular}{lrrrllrrrllrrrl}
& Estimate & St.Err. & p-value &&& Estimate & St.Err. & p-value &&& Estimate & St.Err. & p-value & \\
  \hline
$\beta^{PSII}_{u_1\,p_1}$ & -1.61099 & $45.3\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\beta^{PSII}_{u_6\,p_5}$ & -0.18294 & $44\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\zeta^{PSII}_{u_4}$ & -0.02557 & $3.7\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ \\
$\beta^{PSII}_{u_2\,p_1}$ & -0.82332 & $47.1\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\beta^{PSII}_{u_7\,p_5}$ & -0.13394 & $43.5\,\text{\textperthousand}$ & $2.1\,\text{\textperthousand}$ & $**$ & $\zeta^{PSII}_{u_5}$ & -0.02174 & $3.7\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ \\
  $\beta^{PSII}_{u_3\,p_1}$ & -0.44494 & $45.2\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\eta^{PSII}_{u_1}$ & -0.00603 & $3.9\,\text{\textperthousand}$ & $120.9\,\text{\textperthousand}$ & $$ & $\zeta^{PSII}_{u_6}$ & -0.01746 & $3.6\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ \\
  $\beta^{PSII}_{u_4\,p_1}$ & -0.27184 & $44.5\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\eta^{PSII}_{u_2}$ & 0.03041 & $4\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\zeta^{PSII}_{u_7}$ & -0.01438 & $3.6\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ \\
  $\beta^{PSII}_{u_5\,p_1}$ & -0.17931 & $44.1\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\eta^{PSII}_{u_3}$ & 0.04595 & $3.9\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\delta^{PSII}_{u_1}$ & -0.00032 & $< 0.5\,\text{\textperthousand} $ & $5.6\,\text{\textperthousand}$ & $**$ \\
  $\beta^{PSII}_{u_6\,p_1}$ & -0.13365 & $43.7\,\text{\textperthousand}$ & $2.2\,\text{\textperthousand}$ & $**$ & $\eta^{PSII}_{u_4}$ & 0.05053 & $3.8\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\delta^{PSII}_{u_2}$ & -0.00054 & $< 0.5\,\text{\textperthousand} $ & $< 0.5\,\text{\textperthousand} $ & $***$ \\
  $\beta^{PSII}_{u_7\,p_1}$ & -0.08322 & $43.2\,\text{\textperthousand}$ & $54.1\,\text{\textperthousand}$ & $.$ & $\eta^{PSII}_{u_5}$ & 0.04948 & $3.8\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\delta^{PSII}_{u_3}$ & -0.00056 & $< 0.5\,\text{\textperthousand} $ & $< 0.5\,\text{\textperthousand} $ & $***$ \\
  $\beta^{PSII}_{u_1\,p_3}$ & -1.63933 & $45.3\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\eta^{PSII}_{u_6}$ & 0.04663 & $3.7\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\delta^{PSII}_{u_4}$ & -0.00032 & $< 0.5\,\text{\textperthousand} $ & $3.7\,\text{\textperthousand}$ & $**$ \\
  $\beta^{PSII}_{u_2\,p_3}$ & -0.84804 & $47.2\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\eta^{PSII}_{u_7}$ & 0.04234 & $3.7\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\delta^{PSII}_{u_5}$ & -0.00021 & $< 0.5\,\text{\textperthousand} $ & $53\,\text{\textperthousand}$ & $.$ \\
  $\beta^{PSII}_{u_3\,p_3}$ & -0.44664 & $45.3\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\xi^{PSII}_{u_1}$ & 0.00018 & $< 0.5\,\text{\textperthousand} $ & $28.3\,\text{\textperthousand}$ & $*$ & $\delta^{PSII}_{u_6}$ & -0.00016 & $< 0.5\,\text{\textperthousand} $ & $143.2\,\text{\textperthousand}$ & $$ \\
  $\beta^{PSII}_{u_4\,p_3}$ & -0.27243 & $44.7\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\xi^{PSII}_{u_2}$ & -0.00051 & $< 0.5\,\text{\textperthousand} $ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\delta^{PSII}_{u_7}$ & -0.00011 & $< 0.5\,\text{\textperthousand} $ & $289.8\,\text{\textperthousand}$ & $$ \\
  $\beta^{PSII}_{u_5\,p_3}$ & -0.18370 & $44.3\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\xi^{PSII}_{u_3}$ & -0.00079 & $< 0.5\,\text{\textperthousand} $ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\theta^{PSII}_{u_1}$ & -0.00024 & $< 0.5\,\text{\textperthousand} $ & $69.6\,\text{\textperthousand}$ & $.$ \\
  $\beta^{PSII}_{u_6\,p_3}$ & -0.13633 & $43.8\,\text{\textperthousand}$ & $1.9\,\text{\textperthousand}$ & $**$ & $\xi^{PSII}_{u_4}$ & -0.00085 & $< 0.5\,\text{\textperthousand} $ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\theta^{PSII}_{u_2}$ & 0.00083 & $< 0.5\,\text{\textperthousand} $ & $< 0.5\,\text{\textperthousand} $ & $***$ \\
  $\beta^{PSII}_{u_7\,p_3}$ & -0.08614 & $43.4\,\text{\textperthousand}$ & $47\,\text{\textperthousand}$ & $*$ & $\xi^{PSII}_{u_5}$ & -0.00082 & $< 0.5\,\text{\textperthousand} $ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\theta^{PSII}_{u_3}$ & 0.00076 & $< 0.5\,\text{\textperthousand} $ & $< 0.5\,\text{\textperthousand} $ & $***$ \\
  $\beta^{PSII}_{u_1\,p_5}$ & -1.63166 & $45.5\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\xi^{PSII}_{u_6}$ & -0.00076 & $< 0.5\,\text{\textperthousand} $ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\theta^{PSII}_{u_4}$ & 0.00057 & $< 0.5\,\text{\textperthousand} $ & $< 0.5\,\text{\textperthousand} $ & $***$ \\
  $\beta^{PSII}_{u_2\,p_5}$ & -0.85456 & $47.4\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\xi^{PSII}_{u_7}$ & -0.00067 & $< 0.5\,\text{\textperthousand} $ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\theta^{PSII}_{u_5}$ & 0.00035 & $< 0.5\,\text{\textperthousand} $ & $6.2\,\text{\textperthousand}$ & $**$ \\
  $\beta^{PSII}_{u_3\,p_5}$ & -0.47570 & $45.5\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\zeta^{PSII}_{u_1}$ & 0.02973 & $3.7\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\theta^{PSII}_{u_6}$ & 0.00021 & $< 0.5\,\text{\textperthousand} $ & $98.6\,\text{\textperthousand}$ & $.$ \\
  $\beta^{PSII}_{u_4\,p_5}$ & -0.31349 & $44.9\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\zeta^{PSII}_{u_2}$ & -0.02344 & $3.9\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\theta^{PSII}_{u_7}$ & 0.00011 & $< 0.5\,\text{\textperthousand} $ & $380.1\,\text{\textperthousand}$ & $$ \\
  $\beta^{PSII}_{u_5\,p_5}$ & -0.22837 & $44.4\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & $\zeta^{PSII}_{u_3}$ & -0.02335 & $3.8\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ & \\
   \hline
\end{tabular}\caption{\textbf{Results of estimating the parameters of \textcolor{mc}{MODEL $8$}.} Continuation from previous page. }\label{bigModelPSIIestim}
\end{table}
\end{landscape}

\begin{table}[ht]
\centering
\begin{tabular}{rrlll}
 & Estimate & St.Err. & p-valueue &  \\
  \hline
 $\gamma_{p_1}$ & 5.289 & $21.6\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ \\
   $\gamma_{p_3}$ & 5.622 & $22.3\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ \\
   $\gamma_{p_5}$ & 5.344 & $22.6\,\text{\textperthousand}$ & $< 0.5\,\text{\textperthousand} $ & $***$ \\
  \hline
\end{tabular}\caption{\textbf{Results of estimating the parameters of \textcolor{mc}{MODEL $8$}.} Continuation from previous page. }\label{bigModelPrecisionEstim}
\end{table}

